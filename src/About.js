import React from "react";
import ReactDOM from 'react-dom';
const About = () => {
    return ( 
        <div className="about">
            <h2>About Us</h2>
            <p>
            The About Us page of your website is an essential source of information for all who want to know more about your business.

            About Us pages are where you showcase your history, what is unique about your work, your company's values, and who you serve.

            The design, written content, and visual or video elements together tell an important story about who you are and why you do it.

            How can you make the most of this integral part of your marketing strategy?

            In this article, you'll learn what makes an exceptional About Us page and find 25 about us page examples of the best ones out there to inspire your own About Us page design and content.

            The Components Of A Great About Us Page
            There isn't a winning template to create a great About Us page. However, there are key components to make a convincing pitch with your brand story.

            Your Mission
            You don't need to outright say, “our mission is ____,” but you should convey the mission of your business in your About Us copy. This is key for attracting talent, as well as leads that have Corporate Social Responsibility (CSR) goals.

            Your Story (History)
            Every business has an origin story worth telling, and usually, one that justifies why you even do business and have clients.

            Some centennial enterprises have pages of content that can fit in this section, while startups can tell the story of how the company was born, its challenges, and its vision for the future.
      
            </p>
        </div>
     );
}
 
export default About;