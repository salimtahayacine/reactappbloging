
import Navbar from './Navbar';
import Home from './Home';
import { useEffect } from 'react';
import About from './About';
import {BrowserRouter as Router ,Route,Routes} from 'react-router-dom';
import Login from './Login';
import CreatBlg from './CreatBlg';
import Register from './Register';
import BlogDetails from './BlogDetails';
import NotFound from './NotFound';

function App() {
  //we can declare some variable here and some javascript after retur n it 
  //const title = 'this is starter blog to revise react tricks ';
  // let persone = {name : 'salim',age : 29}
  // const link= 'http://www.google.com';
  return (
    <Router>
      <div className="App">
      <Navbar />
      <div className="content">
        <Routes>
            <Route  path='/' element ={<Home />} ></Route>
            <Route path='/login' element = {<Login />}></Route>
            <Route path='/about' element = {<About />}></Route>
            <Route path='/register' element = {<Register />}></Route>
            <Route path='/creatBlg' element = {<CreatBlg />}></Route>
            <Route path='/blogs/:id' element = {<BlogDetails />}></Route>
            <Route  path='*' element ={<NotFound />} ></Route>
        </Routes>
        


        {/* React c'ant call boolean & object like this exemple below  */}
      {/* <p> I'm { persone }</p> */}
      {/* <p>{'this is string : My name is taha yacine salim '}</p>
      <p>{"this is anumber : "+30}</p>
      <p>{ [1,2,3,4]}</p> */}
      {/* <a href={link}>this is a link </a> */}
      
      </div>
    </div>
    </Router>
  );
}

export default App;
