import { useState } from "react";
import {useHistory,useNavigate} from 'react-router-dom';

const CreatBlg = () => {
    
    const [title,setTitle] = useState('');
    const [body,setBody] = useState('');
    const [author,setAuthor] = useState('salim');
    const [isloading,setIsLoading] = useState(false);
    //const history = useHistory();
    const navigate = useNavigate();
    const handleSubmit = (e) => {
        e.preventDefault();
        const blog = {title , body , author}
        setIsLoading(true);
        fetch('http://localhost:8000/blogs',{
            method: 'POST',
            headers : { "content-Type" : "application/json" },
            body : JSON.stringify(blog)
        }).then(()=> {
            console.log('new blog Added');
            setIsLoading(false);
            //In react-router-dom v6 useHistory() is replaced by useNavigate().
            //history.go(1);
            //history.push('/');
            // import { useNavigate } from 'react-router-dom';
            // const navigate = useNavigate();
            // navigate('/home');
            navigate('/');
        });
    }
    return ( 
        <div className="create">
            <h2>Add New Blog</h2><br/>
            <form onSubmit={handleSubmit}>
                <label>Blog Title : </label> 
                <input
                 type="text"
                 value={title}
                 onChange={(e)=> setTitle(e.target.value)}
                  required/><br/>
                  <label>Blog Body : </label>
                  <textarea
                  value={body}
                  onChange={(e)=> setBody(e.target.value)}
                  required
                  ></textarea><br/>
                  <label>Select Author : </label> 
                  <select 
                  value={author}
                  onChange={(e)=> setAuthor(e.target.value)}
                  required>
                    <option value="Salim">Salim</option>
                    <option value="Yoshi">Yoshi</option>
                    <option value="Mario">Mario</option>
                  </select><br />
                  {!isloading && <button >Add Blog</button>}
                  {isloading && <button disabled >Is Loading........</button>}
            </form>
            <p>{title}</p>
            <p>{body}</p>
            <p>{author}</p>
        </div>
     );
}
 
export default CreatBlg;