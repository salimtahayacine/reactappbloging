import { Link } from "react-router-dom";

const Navbar = () => {
    return ( 
        <nav className="navbar">
            <h1><a href="/">Stronglover Blog</a></h1>
            <div className="links">
                {/* we can use Link from react router dom like Link to="" instead of a href */}
                <Link to="/" >Home</Link>
                <Link to="/login" >Login</Link>
                <Link to="/about" >About</Link>
                <Link style={{color:"white"}} className="btn-create-blog" to="/creatBlg" >New Blog <span style={{color:"black"}}>+</span></Link>
            </div>
            
        </nav>
     );
}
 //we can also make style css by using style ={{ css here }}
export default Navbar;