import { Link } from "react-router-dom";
const NotFound = () => {
    return ( 
        <div className="not-found">
            <h3>Sorry Page Not Found 
                <span style={{color : "red"}}> ERROR-404 </span></h3>
                <Link to="/">Go back to home page</Link>
        </div>
     );
}
 
export default NotFound;